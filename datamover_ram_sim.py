#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 11:18:37 2024

@author: robert
"""

import numpy as np
import matplotlib.pyplot as plt
from enum import Enum, auto

plt.close('all')

# time in secs
# data in MBs

# parameters
max_bw_server = 4 * 1024
n_threads = 40
fread = 150 # bw to eos
fwrite = max_bw_server / n_threads
fsize = 5 * 1024
time = 3600

blocking = False

t = list(range(0, time))

## what we have now
# curve generation
stream_pct = np.array([55, 29, 2.5, 10, 1.5, 2])
stream = stream_pct / 100

m = stream * fwrite

copy_time = int(fsize / fread)

class EosCp(Enum):
    idle = auto()
    copying = auto()

def ram_usage(t, ibw):
    accu = 0
    f_cntr = 1 
    pipe_cntr = 0
    cp_cntr = 0
    r = np.zeros(t)
        
    cp_state = EosCp.idle
    
    for i in range(1, t):
        accu += ibw
        r[i] += r[i-1] + ibw
        if accu > f_cntr * fsize:
            f_cntr += 1
            pipe_cntr += 1
        
        if cp_state == EosCp.idle:
            if pipe_cntr:
                cp_cntr = copy_time
                cp_state = EosCp.copying
        elif cp_state == EosCp.copying:
            cp_cntr -= 1
            if cp_cntr == 0:
                cp_state = EosCp.idle
                pipe_cntr -= 1
                r[i] -= fsize
        
        i += 1
        
    return r

def ram_usage_blocking(t, ibw):
    cp_cntr = 0
    r = np.zeros(t)
    
    cp_state = EosCp.idle
    
    for i in range(1, t):
        if cp_state == EosCp.idle:
            r[i] += r[i-1] + ibw
            if r[i] >= fsize:
                r[i] = fsize
                cp_cntr = copy_time
                cp_state = EosCp.copying
        elif cp_state == EosCp.copying:
            cp_cntr -= 1
            r[i] = r[i-1]
            if cp_cntr == 0:
                cp_state = EosCp.idle
                r[i] -= fsize
    
    return r

# ram usage per file and thread
plt.figure()

ram_usages = []
thread_ram = np.full((time,), 0.0)
for i in m:
    if blocking:
        r = ram_usage_blocking(time, i)
    else:
        r = ram_usage(time, i)
    thread_ram += r
    plt.plot(r)
    ram_usages.append(r)


plt.title('RAM curve / file')
plt.xlabel('time [s]')
plt.ylabel('size [MB]')

plt.figure()
plt.plot(t, thread_ram / 1024)
plt.title('STAGE usage / thread')
plt.xlabel('time [s]')
plt.ylabel('size [GB]')

# all threads
plt.figure()
plt.plot(t, thread_ram * n_threads / 1024)
plt.title(f'STAGE usage / {n_threads} threads')
plt.xlabel('time [s]')
plt.ylabel('size [GB]')

## every mover write the same file
reader_num = 28
fwr = max_bw_server * stream
accu = np.zeros(len(m))
pipes = np.empty(0)
thread_ram2 = np.full((time,), 0.0)

for ti in range(time):
    accu += fwr
    
    for i, v in enumerate(accu):
        if v >= fsize and len(pipes) < reader_num:
            accu[i] -= fsize
            pipes = np.append(pipes, fsize)
    
    pipes -= fread
    pipes = pipes[pipes > 0]
    thread_ram2[ti] = sum(accu) + len(pipes) * fsize

plt.figure()
plt.plot(t, thread_ram2 / 1024)
plt.title(f'STAGE usage with method 2 with {reader_num} eos readers')
plt.xlabel('time [s]')
plt.ylabel('size [GB]')


